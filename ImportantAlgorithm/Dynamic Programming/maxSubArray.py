def naiveApproach(listValue):
    maxValue=-9999999999999999999999999999999999999999999999999
    for i in range(len(listValue)):
        sumValue=0
        for j in range(i+1,len(listValue)):
            listValue[i]+=listValue[j]
            maxValue=max(maxValue,listValue[i])
    print(maxValue)
listValue=[8,-8,5,8,-2,11,-13]

#Kadane's Algorithm
def kadanesAlgorithm(listVal):
    currentMax=-99999999999999999999999999999999999
    maxValueAll=-999999999999999999999999999999
    for i in range(len(listValue)):
        value=listVal[i]
        currentMax=max(listValue[i],currentMax+listValue[i])
        maxValueAll=max(maxValueAll,currentMax)
    print(maxValueAll)
kadanesAlgorithm(listValue)

def maxSubarray(array,left,right):
    if left==right:
        return listValue[left]
    mid=(left+right)//2
    
    add=0
    leftMax=-999
    for i in range(mid,left-1,-1):
        add+=array[i]
        if add>leftMax:
            leftMax=add
    second=0
    rightMax=-99999
    for j in range(mid+1,right+1):
        second+=array[j]
        if second>rightMax:
            rightMax=second
    maxLeftRight=max(maxSubarray(array,left,mid),maxSubarray(array,mid+1,right))
    return max(maxLeftRight,leftMax+rightMax)
def crossAddition(array,mid,right,left):
    sum=0
    leftMax=-9999
    for i in range(mid-1,left,-1):
        sum+=array
        leftMax=max(leftMax,sum)
    sum=0
    rightMax=-99999
    for i in range(mid,right):
        sum+=array
        rightMax=max(rightMax,sum)
    return max(rightMax,leftMax,leftMax+rightMax)
    
def separate(array,left,right):
    if left==right:
        return array[left]
    mid=len(array)//2
    leftRight=max(separate(array,0,mid),separate(array,mid+1,len(array)))
    maxValue=max(leftRight,crossAddition(array,mid,right,left))
    return maxValue
print(separate(listValue,0,len(listValue)-1))
        