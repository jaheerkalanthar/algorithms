from typing import List


def sumOfSubSet(target,listValue):
    tableOfValues=[[0 for item in range(target+1)] for _ in range(len(listValue))]
    for i in range(len(listValue)):
        for j in range(target+1):
            tableOfValues[i][0]=1#Becaust the No value can make specific values
            if listValue[i]<j and i!=0:
                tableOfValues[i][j]=tableOfValues[i-1][j]
            if listValue[i]==j:
                tableOfValues[i][j]=1#Because 1 value can make specific values
            
            if i!=0:
                tableOfValues[i][j]=tableOfValues[i-1][j]+tableOfValues[i][j-listValue[i]]
            #print(tableOfValues[i][7-4])
    print(tableOfValues)
ListValue=[3,4,5,6]
target=9
sumOfSubSet(target,ListValue)
