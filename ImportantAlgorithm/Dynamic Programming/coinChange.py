def coinChange(target,coins):
    table=[[0 for i in range(target+1)] for j in range(len(coins))]
    for i in range(len(coins)):
        for j in range(target+1):
            table[i][0]=1
            if coins[i]>j:
                table[i][j]=table[i-1][j]
            else:
                table[i][j]=table[i-1][j]+table[i][j-coins[i]]
    print(table)
target=15
coins=[2,3,5,10]#Only TwoWays to achive 7 (2+5=7),(2+2+3)=7
coinChange(target,coins)