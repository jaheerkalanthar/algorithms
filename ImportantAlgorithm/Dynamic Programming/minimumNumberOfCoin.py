def minimumNumberOfCoin(target,coins):
    tableContent=[[0 for _ in range(target+1)] for item in range(len(coins))]
    for i in range(len(coins)):
        for j in range(target+1):
            if j==0:
                tableContent[i][j]=0
            elif i==0:
                tableContent[i][j]=j
            elif coins[i]>j:
                tableContent[i][j]=tableContent[i-1][j]
            else:
                tableContent[i][j]=min(1+tableContent[i][j-coins[i]],tableContent[i-1][j])
    print(tableContent)
target=11
coins=[1,5,6,9]
minimumNumberOfCoin(target,coins)