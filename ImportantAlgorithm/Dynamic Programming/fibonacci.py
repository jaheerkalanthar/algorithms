def fibonacciMyApproach(n):
    a=0
    b=1
    for i in range(n):
        c=a+b
        b=a
        a=c
        print(c)
def fibonacciRecursiveApproach(n):# This One is Not efficient
    if n==0:
        return 0
    elif n==1:
        return 1
    else:
        return fibonacciRecursiveApproach(n-1)+fibonacciRecursiveApproach(n-2)
def BottomUpApproach(n):
    memo=[0 for _ in range(n+1)]
    memo[0]=0
    memo[1]=1
    for i in range(2,n+1):
        if memo[i] in memo: 
            memo[i]=memo[i-1]+memo[i-2]

    return memo
def memoization(n,memo):
    if n in memo:
        return memo[n]
    if n==0:
        return 0
    if n==1:
        return 1
    else:
       memo[n]= memoization(n-1,memo)+memoization(n-2,memo)
       return memo[n]
def withUsinList(n,memo):
    if n<=2:
        memo[n]=1
    
    if memo[n]==-1:
        sumValue=(withUsinList(n-1,memo))+(withUsinList(n-2,memo))
        memo[n]=sumValue
        return sumValue
    else:
        return memo[n]
n=1000

print(withUsinList(n,[-1 for _ in range(n+1)]))
