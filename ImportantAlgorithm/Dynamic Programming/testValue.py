def gridMove(m,n):
    if n==1 or m==1:
        return 1
    else:
        return gridMove(m-1,n)+gridMove(m,n-1)
def efficientSolution(m,n,memoization):
    key=(m,n)
    if key in memoization:
        return memoization[key]
    if n==1 or m==1:
        return 1
    else:
        memoization[key]= efficientSolution(m-1,n,memoization)+ efficientSolution(m,n-1,memoization)
        return memoization[key]
print(efficientSolution(20,20,{}))
