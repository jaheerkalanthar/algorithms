def gridMovementNaiveApproach(m,n):
    if n==1 and m==1:
        return 1
    elif n==0 or m==0:
        return 0
    else:
        return gridMovementNaiveApproach(m-1,n)+gridMovementNaiveApproach(m,n-1)#It will do operation 2**m+n times

def efficientApproach(m,n,memoization):
    key=(m,n)
    if key in memoization:
        return memoization[key]
    if n==1 and m==1:
        return 1
    elif n==0 or m==0:
        return 0
    else:
        memoization[key]=efficientApproach(m-1,n,memoization)+efficientApproach(m,n-1,memoization)#It will do operation 2**m+n times
        return memoization[key]

