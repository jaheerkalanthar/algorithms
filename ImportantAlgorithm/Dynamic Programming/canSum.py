def canSum(target,listValue):
    storage={}
    if target==0:
        return True
    if target<0:
        return False
    else:
        for i in listValue:
        # values=listValue[i]
            remainder=target-i
            if canSum(remainder,listValue)==True:
                return True
        return False
target,listValue=7,[2,3]
def dynamicApproachOfSameProblem(target,listValue):
    table=[[0 for _ in range(target+1)] for item in range(len(listValue))]
    for i in range(len(listValue)):
        for j in range(target+1):
            if listValue[i]==j:
                table[i][j]=1
            elif j==0:
                table[i][0]=1
            elif listValue[i]>j:
                table[i][j]=table[i-1][j]
            else:
                table[i][j]=table[i-1][j]+table[i][j-listValue[i]]
    print(table)
def dynamicApproachinRecursion(target,listValue,memo):
    if target==0:
        return True
    elif target<0:
        return False
    else:
        if target not in memo:
            for i in range(len(listValue)):
                values=listValue[i]
                represent=target-values
                
                if dynamicApproachinRecursion(represent,listValue,memo)==True:
                    memo[represent]=True
                    print(memo)
                    return memo[represent]
                memo[represent]= False
                print(memo)
                return memo[represent]

def returnNumberOfValue(target,listValue,memo,Values):


    if target==0:
        return []
    if target<1:
        return None
    
    for i in range(len(listValue)):
            values=listValue[i]
            represent=target-values
            Values.append(values)
            val= returnNumberOfValue(represent,listValue,memo,Values)
            if val!=None:
                return Values
    return None
print(returnNumberOfValue(target,listValue,{},[]))