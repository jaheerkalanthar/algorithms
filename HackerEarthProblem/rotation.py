def rotation(noValue):
    for _ in range(noValue):
        n,k=map(int,input().split(" "))
        listValue=input().split(" ")
        x=int(k)%int(n)
        listValue=listValue[n-x:]+listValue[:n-x]
    joinedValue=" ".join(listValue)
    print(joinedValue)
noValue=int(input())
rotation(noValue)